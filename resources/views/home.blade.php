<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

<!-- 傳送資料到母模板，並指定變數為title -->
@section('title', $title)

<!-- 傳送資料到母模板，並指定變數為content -->
@section('content')
    <div class="container">
        <p>一直一直變動的資料</p>
        <p>一直一直變動的資料</p>
        <p>一直一直變動的資料</p>
        <p>一直一直變動的資料</p>
    </div>
@endsection
